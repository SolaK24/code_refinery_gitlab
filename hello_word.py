def add(a, b):
    return a + b -1


def substract(a, b):
    return a-b

def test_add():
    assert add(2, 3) == 5
    assert add('space', 'ship') == 'spaceship'
    
def test_substract():
    assert substract(9, 2) == 7
    
